# Express + Angular + Node.js (EAN)

## System requirements
Make sure you have the following requirements installed before starting this project.

#### Node.js
To install node.js in Ubuntu 16.04 follow the instructions [here](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04#how-to-install-using-a-ppa). To install node.js in Ubuntu 14.04 follow the instructions [here](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server#how-to-install-using-a-ppa). To install Node.js on Windows download it from the official page [nodejs.org](https://nodejs.org/en/).

#### Bower
Bower is used for managing front-end dependencies. Make sure you have Node.js installed before installing Bower. To install Bower run: 
`sudo npm install -g bower`

#### Gulp
Gulp is task automation tool used for running useful tasks for code linting, JavaScript/CSS concatination and minification, script inject etc. To install it run: 
`sudo npm install -g gulp-cli`

*Note: If you are on Windows `sudo` is not required.

## Install dependencies
To install all back-end project dependencies run in the root folder:
`npm install`

To install all front-end dependencies run in the root folder:
`bower install`

These commands will install all project dependencies and place them in their folders. By default Bower dependencies will be placed in /bower_components and node dependencies will be placed in /node_modules.

## Setup
Since the project contains both front-end and back-end logic it's separated into two areas - client and server.

The client folder contains the front-end code and the server contains the code executed on the server side.

----
1. What is Angular?

1. Why Angular?

1.  Project Setup
    1. Installing Node.js and NPM
    1. Installing Bower.js
    1. Folder structure

1. Setup Gulp
    1. Working with SCSS

1. Creating Angular module
    1. Module definition
    1. HTML markup

1. Creating a controller

1. Installing Angular dependencies
  1. Installing angular ui router
  1. Using $stateProvider in router

1. Defining project routes
    1. Creating routes with parametars

1. Creating abstract angular ui routes
    1. Defining 'app' core route

1. Sending custom data from the router
    1. Page title example

1. Working with templates
    1. ng-repeat
    1. conditions
    1. ng-src
    1. ng-class
    1. ng-hide/ng-show/ng-if
    1. ng-bind

1. Defining and working with directives
    1. With/without replace
    1. template/templateUrl
    1. scope
    1. restrict

1. Creating API service

1. Creating custom filter

1. Form validation
    1. Adding Angular validation to a form