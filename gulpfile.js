var gulp = require('gulp'),
    rimraf = require('rimraf');

gulp.task('clean:dev', function (cb) {
  return rimraf('./client/.tmp', cb);
});

gulp.task('clean:prod', function (cb) {
  return rimraf('./client/build', cb);
});


gulp.task('serve', ['clean:dev'], function () {
  require('./tasks/development');
  gulp.start('build');
});

gulp.task('build', ['clean:prod'], function () {
  require('./tasks/production');
  gulp.start('build');
});