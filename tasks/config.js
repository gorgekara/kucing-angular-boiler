module.exports = {
  path: {
    tmp: './client/.tmp',
    src: './client/src',
    build: './client/build'
  }
};