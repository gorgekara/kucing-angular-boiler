var config = require('./config'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    inject = require('gulp-inject'),
    concat = require('gulp-concat'),
    copy = require('gulp-contrib-copy'),
    bowerFiles = require('main-bower-files'),
    browserSync = require('browser-sync').create();

gulp.task('copy', function () {
  return gulp.src(config.path.src + '/**/*')
    .pipe(gulp.dest(config.path.tmp));
});

gulp.task('scss', ['copy'], function () {
  return gulp.src([
      config.path.src + '/app/components/_core/_core.scss',
      config.path.src + '/app/**/*.scss'
    ])
    .pipe(concat('main.scss'))
    .pipe(sass())
    .pipe(gulp.dest(config.path.tmp + '/public/css'))
    .pipe(browserSync.stream());
});

gulp.task('lint', ['copy'], function () {
  return gulp.src(config.path.tmp + '/app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('inject', ['copy', 'scss'], function () {
  var sources = gulp.src(bowerFiles().concat([
    config.path.tmp + '/public/css/*.css',
    config.path.tmp + '/app/app.module.js',
    config.path.tmp + '/app/*.js',
    config.path.tmp + '/app/**/*.js'
  ]));

  return gulp.src(config.path.src + '/index.html')
    .pipe(inject(sources, {
        relative: true,
        transform: function (filepath, file, i, length) {
          var filepathSplit = filepath.split('.'),
              filetype = filepathSplit[filepathSplit.length - 1],
              newFilePath = filepath.replace('../.tmp/', '');

          if (filetype === 'css') {
            return '<link rel="stylesheet" href="' + newFilePath + '" />';
          } else if (filetype === 'js') {
            return '<script type="text/javascript" src="' + newFilePath + '"></script>';
          }
        }
      }))
    .pipe(gulp.dest(config.path.tmp));
});

gulp.task('build', ['copy', 'scss', 'lint', 'inject'], function () {
  browserSync.init({
    server: {
      baseDir: config.path.tmp
    }
  });

  gulp.watch(config.path.src + '/app/**/*.scss', ['scss']);
  gulp.watch([config.path.src + '/app/**/*.html', config.path.src + '/app/**/*.js'], ['lint','inject']).on('change', browserSync.reload);
});